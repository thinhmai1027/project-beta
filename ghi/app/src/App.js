import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import SalesList from './Sales/SalesList.js'
import SalesForm from './Sales/SalesForm.js'
import CustomerForm from './Sales/CustomerForm.js'
import SalesPersonForm from './Sales/SalesPersonForm.js'
import SalesHistoryForm from './Sales/SalesHistory.js'
import ManufacturerList from './Inventory/ManufacturersList.js'
import VehicleModelList from './Inventory/VehicleModelsList.js'
import AutoList from './Inventory/AutoList.js'
import ManufacturerForm from './Inventory/ManufacturerForm.js'
import VehicleModelForm from './Inventory/VehicleModelForm.js'
import AutoForm from './Inventory/AutoForm.js'
import React from 'react';
import ServiceForm from './ServiceForm';
import ServiceList from './ServiceList';
import TechnicianForm from './TechnicianForm';
import ServiceHistory from './ServiceHistory';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="sales">
            <Route path="" element={<SalesList />}/>
            <Route path="new" element={<SalesForm />} />
          </Route>
          <Route path="customer">
            <Route path="new" element={<CustomerForm />} />
          </Route>
          <Route path="salesperson">
            <Route path="new" element={<SalesPersonForm />} />
          </Route>
          <Route path="salesperson">
            <Route path="history" element={<SalesHistoryForm />} />
          </Route>
          <Route path="manufacturers">
            <Route path="" element={<ManufacturerList />}/>
            <Route path="new" element={<ManufacturerForm />} />
          </Route>
          <Route path="vehiclemodels">
            <Route path="" element={<VehicleModelList />}/>
            <Route path="new" element={<VehicleModelForm />}/>
          </Route>
          <Route path="autos">
            <Route path="" element={<AutoList />}/>
            <Route path="new" element={<AutoForm />}/>
          </Route>
          <Route path="services/new" element={<ServiceForm />} />
          <Route path="services_list" element={<ServiceList/>} />
          <Route path="services_history" element={<ServiceHistory />} />
          <Route path="technician/new" element={<TechnicianForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;