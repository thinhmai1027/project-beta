import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">
          CarCar
        </NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="/">Home</NavLink>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="/sales" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">Sales</a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                <NavLink className="dropdown-item" aria-current="page" to="/sales/">Sales Records</NavLink>
                <NavLink className="dropdown-item" aria-current="page" to="/sales/new/">Record a sale record</NavLink>
                <div className="dropdown-divider"></div>
                <NavLink className="dropdown-item" aria-current="page" to="/salesperson/new">New Sales Person</NavLink>
                <NavLink className="dropdown-item" aria-current="page" to="/salesperson/history">Sales Person History</NavLink>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="/sales" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">Inventory</a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                <NavLink className="dropdown-item" aria-current="page" to="/manufacturers/">Manufacturers</NavLink>
                <NavLink className="dropdown-item" aria-current="page" to="/manufacturers/new/">Add a manufacturer</NavLink>
                <div className="dropdown-divider"></div>
                <NavLink className="dropdown-item" aria-current="page" to="/vehiclemodels">Vehicle Models</NavLink>
                <NavLink className="dropdown-item" aria-current="page" to="/vehiclemodels/new">Add a vehicle model</NavLink>
                <div className="dropdown-divider"></div>
                <NavLink className="dropdown-item" aria-current="page" to="/autos">Automobiles</NavLink>
                <NavLink className="dropdown-item" aria-current="page" to="/autos/new/">Add new automobile</NavLink>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                href="#"
                id="navbarDropdown"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Services
              </a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                <NavLink
                  className="dropdown-item"
                  aria-current="page"
                  to="technician/new"
                >
                  Add a Technician
                </NavLink>
                <NavLink
                  className="dropdown-item"
                  aria-current="page"
                  to="services/new/"
                >
                  Create a Sevice Appointment
                </NavLink>
                <NavLink
                  className="dropdown-item"
                  aria-current="page"
                  to="services_list/"
                >
                  Service Appointments
                </NavLink>
                <NavLink
                  className="dropdown-item"
                  aria-current="page"
                  to="services_history/"
                >
                  Service History
                </NavLink>
                <NavLink
                  className="dropdown-item"
                  aria-current="page"
                  to="services_history/"
                ></NavLink>
              </ul>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="/customer/new">New Customer</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;