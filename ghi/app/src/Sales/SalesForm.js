import React from 'react';

class SalesForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      sales_persons: [],
      customers: [],
      automobiles: [],
      sale_price: '',
      sales_person: ''
    }
    this.handleSalesPersonChange = this.handleSalesPersonChange.bind(this)
    this.handleCustomerChange = this.handleCustomerChange.bind(this)
    this.handleAutomobileChange = this.handleAutomobileChange.bind(this)
    this.handleSalePrice = this.handleSalePrice.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleSalesPersonChange(event) {
    const value = event.target.value;
    this.setState({ sales_person: value })
  }
  handleCustomerChange(event) {
    const value = event.target.value;
    this.setState({ customer: value })
  }
  handleAutomobileChange(event) {
    const value = event.target.value;
    this.setState({ automobile: value })
  }
  handleSalePrice(event) {
    const value = event.target.value;
    this.setState({ sale_price: value })
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };
    delete data.automobiles
    delete data.customers
    delete data.sales_persons

    const url = 'http://localhost:8090/api/sales/'
    const fetchConfig = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        }
    }
    const response = await fetch(url, fetchConfig)

    if (response.ok){
      const newForm = await response.json()
    }
    const cleared = {
      sales_person: [],
      customers: [],
      automobile: [],
      sale_price: '',
    }
    this.setState(cleared);
  }

  async componentDidMount() {
    const sales_person_url = 'http://localhost:8090/api/salesperson/'
    const automobile_url = 'http://localhost:8090/api/automobileVO/'
    const customer_url = 'http://localhost:8090/api/customer/'
    const sales_response = await fetch(sales_person_url)
    const auto_response = await fetch(automobile_url)
    const customer_response = await fetch(customer_url)

    if (sales_response.ok) {
      const data = await sales_response.json();
      this.setState({ sales_persons: data.sales_persons })
    }
    if (auto_response.ok) {
      const data = await auto_response.json();
      this.setState({ automobiles: data.automobiles })
    }
    if (customer_response.ok) {
      const data = await customer_response.json();
      this.setState({ customers: data.customers })
    }
  }


  render() {
    return (
      <>
      <br></br>
      <br></br>
      <br></br>
      <br></br>
      <br></br>
      <br></br>
      <br></br>
      <br></br>
      <br></br>
      <br></br>
      <br></br>
      <br></br>
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1 className="text-center">Record a new sale</h1>
            <form onSubmit={this.handleSubmit} id="create-sales-record-form">
              <div className="mb-3">
                <select onChange={this.handleSalesPersonChange} value={this.state.sales_person} name="Sales_person" required id="sales_person" className="form-select">
                  <option value="">Choose a sales person</option>
                  {this.state.sales_persons?.map(sale => {
                    return (
                      <option key={sale.id} value={sale.employee_number}>
                        {sale.name}
                      </option>
                    )
                  })}
                </select>
              </div>
              <div className="mb-3">
                <select onChange={this.handleCustomerChange} value={this.state.customer} name="Customer" required id="customer" className="form-select">
                  <option value="">Choose a customer</option>
                  {this.state.customers?.map(cust => {
                    return (
                      <option key={cust.id} value={cust.id}>
                        {cust.name}
                      </option>
                    )
                  })}
                </select>
              </div>
              <div>
                <select onChange={this.handleAutomobileChange} value={this.state.automobile} name="Automobile" required id="automobile" className="form-select">
                  <option value="">Choose a automobile</option>
                  {this.state.automobiles?.map(auto => {
                    return (
                      <option key={auto.import_href} value={auto.import_href}>
                        {auto.import_vin}
                      </option>
                    )
                  })}
                </select>
              </div>
              <div className="form-floating mb-3" >
                <input value={this.state.sale_price} onChange={this.handleSalePrice} placeholder="Sale-price" required type="number" name="sale_price" id="sale_price" className="form-control" />
                <label htmlFor="sale_price">Sale Price:</label>
              </div >
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
      </>
    )
  }
}

export default SalesForm