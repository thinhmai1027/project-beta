import React from 'react';

class SalesHistoryForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      salesPersons: [],
      salesData: [],
      sales_person: ""
    }
    this.handleSalesPersonChange = this.handleSalesPersonChange.bind(this)
  }
  handleSalesPersonChange(event) {
    const value = event.target.value
    this.setState({ sales_person: parseInt(value) })

  }

  async componentDidMount() {
    const sales_person_url = 'http://localhost:8090/api/salesperson/'
    const sales_url = 'http://localhost:8090/api/sales/'
    const sales_person_response = await fetch(sales_person_url)
    const sales_response = await fetch(sales_url)

    if (sales_person_response.ok) {
      const data = await sales_person_response.json();
      this.setState({ salesPersons: data.sales_persons })
    }
    if (sales_response.ok) {
      const data = await sales_response.json();
      this.setState({ salesData: data.sales })
    }
  }


  render() {
    return (
      <>
      <br></br>
      <br></br>
      <br></br>
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1 className="text-center">Sales History</h1>
              <br></br>
              <form id="create-customer-form">
                <div className="mb-3">
                  <select onChange={this.handleSalesPersonChange} value={this.state.sales_person} name="Sales_person" required id="sales_person" className="form-select">
                    <option className="text-center" value="">Choose a sales person</option>
                    {this.state.salesPersons?.map(sale => {
                      return (
                        <option key={sale.id} value={sale.employee_number}>
                          {sale.name}
                        </option>
                      )
                    })}
                  </select>
                </div>
              </form>
            </div>
          </div>
        </div>
        <br></br>
        <br></br>
        <br></br>
        <br></br>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Sales Person</th>
              <th>Customer</th>
              <th>VIN</th>
              <th>Sale Price</th>
            </tr>
          </thead>
          <tbody>
            {this.state.salesData.filter(salesData => salesData.sales_person.employee_number === this.state.sales_person).map(sales_record => {
              return(
                <tr key={sales_record.id}>
                <td>{sales_record.sales_person.name}</td>
                <td>{sales_record.customer.name}</td>
                <td>{sales_record.automobile.import_vin}</td>
                <td>{sales_record.sale_price}</td>
              </tr>
              )
            })}
          </tbody>
        </table>
      </>
    );
  }
}
export default SalesHistoryForm