import React from 'react'
import './inventory.css'

class VehicleModelList extends React.Component {
  constructor(props){
    super(props)
    this.state= {
      models: []
    }
  }
  async componentDidMount() {
    const url = "http://localhost:8100/api/models/"
    const response = await fetch(url)

    if (response.ok) {
      const data = await response.json()
      this.setState({ models: data.models })
    }
  }

  render() {
    return (
      <>
        <br></br>
        <br></br>
        <br></br>
        <h1 className="dislplay-1 text-center" >Vehicle Models</h1>
        <br></br>
        <br></br>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Picture</th>
              <th>Name</th>
              <th>Manufacturer</th>
            </tr>
          </thead>
          <tbody>
          {this.state.models?.map(model => {
              return (
                <tr key={model.id}>
                  <td><img src={model.picture_url} width={240} height={140}/></td>
                  <td>{model.name}</td>
                  <td>{model.manufacturer.name}</td>
                </tr>
              )
            })}
          </tbody>
        </table>
      </>
    )
  }
}

export default VehicleModelList