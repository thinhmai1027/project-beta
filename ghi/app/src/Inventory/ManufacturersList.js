import React from 'react'

class ManufacturerList extends React.Component {
  constructor(props){
    super(props)
    this.state= {
      manufacturers: []
    }
  }
  async componentDidMount() {
    const url = "http://localhost:8100/api/manufacturers/"
    const response = await fetch(url)

    if (response.ok) {
      const data = await response.json()
      this.setState({ manufacturers: data.manufacturers })
    }
  }

  render() {
    return (
      <>
        <br></br>
        <br></br>
        <br></br>
        <h1 className="dislplay-1 text-center" >Manufacturers</h1>
        <br></br>
        <br></br>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Name</th>
              <th>ID</th>
            </tr>
          </thead>
          <tbody>
          {this.state.manufacturers?.map(manu => {
              return (
                <tr key={manu.id}>
                  <td>{manu.name}</td>
                  <td>{manu.id}</td>
                </tr>
              )
            })}
          </tbody>
        </table>
      </>
    )
  }
}

export default ManufacturerList