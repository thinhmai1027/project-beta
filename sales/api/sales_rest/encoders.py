from common.json import ModelEncoder
from .models import AutomobileVO, SalesPerson, Customer, Sales


class AutomobileVoEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
      "id",
      "import_vin",
      "year",
      "color",
      "import_href"
      # "model",
      # "manufacturer"
    ]

class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
      "id",
      "name",
      "employee_number",
    ]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
      "id",
      "name",
      "phone_number",
      "address"
    ]

class SalesEncoder(ModelEncoder):
    model = Sales
    properties = [
      "id",
      "sale_price",
      "automobile",
      "sales_person",
      "customer"
    ]
    encoders = {
      "automobile": AutomobileVoEncoder(),
      "sales_person": SalesPersonEncoder(),
      "customer": CustomerEncoder()
    }