from django.contrib import admin
from .models import Service, Status, Technician
# Register your models here.

@admin.register(Service)
class ServiceAdmin(admin.ModelAdmin):
    pass

@admin.register(Status)
class StatusAdmin(admin.ModelAdmin):
    pass

@admin.register(Technician)
class TechnicianAdmin(admin.ModelAdmin):
    pass
